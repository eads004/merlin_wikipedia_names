
## Process to identify authors who have been translated ##

A broad outline of the process:

1.  We received a file of HathiTrust metadata named **recordmeta.tsv**, I think from Andrew.

2.  I received Doug's matching results (**title_categorized_DK.csv**) and Andrew's (**Hathi_VIAF_NAmerica_01.csv**).

3.  I ran the processes contained in this repository (notebooks **00_get_names_from_wikipedia.ipynb**, **03_ALL_match_wikipedia.ipynb**, and **04_merge.ipynb**).  Details about these notebooks follow below.

4.  We sent **translations_only.csv** to Andrew (we also sent a second file of category counts to him, but that file is especially important in this process).

5.  Andrew sent us **Original_Hathi.csv** and **Translations_Hathi.csv**, which established our corpora for the analysis.

### 00_get_names_from_wikipedia.ipynb ###

Reads a bunch of Wikipedia list and category pages.  Writes language-specific csv files to folder Hathi_Translations.  These csv files contain the names of the authors who are listed in the Wikipedia pages.

### 03_ALL_match_wikipedia.ipynb ###

Matches the lists of names from 00_get_names_from_wikipedia.ipynb to the names in recordmeta.tsv; and outputs a csv which prepends the results of the matching to the data in recordmeta.tsv.

### 04_merge.ipynb ###

Merges the output from 03_ALL_match_wikipedia.ipynb with the files from Doug and Andrew, and outputs translations_only.csv.



